package com.synvata.datapointsbackend.exception;

public class InvalidTokenException extends BusinessException {

    private static final long serialVersionUID = 1L;

    public InvalidTokenException(String message) {
        super(message);
    }

    public InvalidTokenException(String message, Throwable cause) {
        super(message, cause);
    }
}

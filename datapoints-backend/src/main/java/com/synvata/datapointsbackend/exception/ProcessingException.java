package com.synvata.datapointsbackend.exception;

public class ProcessingException extends BusinessException {

    private static final long serialVersionUID = 1L;

    public ProcessingException(String message) {
        super(message);
    }

    public ProcessingException(String message, Throwable cause) {
        super(message, cause);
    }

}

package com.synvata.datapointsbackend.exception;

public class InvalidDateException extends BusinessException{

    private static final long serialVersionUID = 1L;

    public InvalidDateException(String message) {
        super(message);
    }

    public InvalidDateException(String message, Throwable cause) {
        super(message, cause);
    }
}

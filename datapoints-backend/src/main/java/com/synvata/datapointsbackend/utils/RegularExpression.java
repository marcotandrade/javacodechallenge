package com.synvata.datapointsbackend.utils;

public enum RegularExpression {

    VALIDATE_ONLY_NUMBER("[^=<>\\D]+"),
    VALIDATE_QUARTER_INDEX("\\d{4}\\-\\d{2}\\-\\d{2}");


    public String value;

    RegularExpression(String value) {
        this.value = value;
    }
}

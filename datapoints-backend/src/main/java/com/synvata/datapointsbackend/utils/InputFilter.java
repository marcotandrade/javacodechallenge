package com.synvata.datapointsbackend.utils;

import org.springframework.stereotype.Service;

@Service
public class InputFilter {

    public boolean validateInput(String input, String validationRegex){
        return input.matches(validationRegex);
    }
}

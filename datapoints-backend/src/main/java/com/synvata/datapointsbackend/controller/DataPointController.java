package com.synvata.datapointsbackend.controller;

import com.synvata.datapointsbackend.dto.ProcessResult;
import com.synvata.datapointsbackend.dto.RequestDto;
import com.synvata.datapointsbackend.dto.ResponseDto;
import com.synvata.datapointsbackend.exception.InvalidDateException;
import com.synvata.datapointsbackend.exception.ProcessingException;
import com.synvata.datapointsbackend.service.QuarterIndexService;
import com.synvata.datapointsbackend.utils.InputFilter;
import com.synvata.datapointsbackend.utils.RegularExpression;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.xml.bind.JAXBException;
import java.text.ParseException;
import java.util.List;

@RestController
@RequestMapping("ws")
public class DataPointController {

    private static final Logger log = LoggerFactory.getLogger(DataPointController.class);

    @Autowired
    private InputFilter inputFilter;

    @Autowired
    private QuarterIndexService quarterIndexService;

    /**
     * Process request
     *
     *
     * @param requestDto
     * @return
     */
    @GetMapping
    public ResponseEntity getCompanyDataPoint(@RequestBody RequestDto requestDto) {
        long startTime = System.currentTimeMillis();
        log.info("Initialize request processing for company data points");
        ResponseEntity response = null;
        try {
            if (validateRequestInfo(requestDto)) {
                String[] fileName = quarterIndexService.getIndexList(requestDto);
                List<ResponseDto> result = quarterIndexService.getFileInfo(requestDto, fileName);
                response = new ResponseEntity<>(result, HttpStatus.OK);
            } else {
                log.warn(String.format("Incomplete data received on request object: %s", requestDto.toString()));
                response = new ResponseEntity<>(new ProcessResult("Incomplete data"), HttpStatus.BAD_REQUEST);
            }
            log.info(String.format("Extraction of data points took %s ms", System.currentTimeMillis() - startTime ));
        } catch (ProcessingException ex){
            log.error(ex.getMessage());
            response = new ResponseEntity<>(new ProcessResult("Internal error processing request"), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (InvalidDateException ex){
            log.error(ex.getMessage());
            response = new ResponseEntity<>(new ProcessResult("Invalid Date"), HttpStatus.BAD_REQUEST);
        } catch (JAXBException | ParseException ex) {
            log.error(ex.getMessage());
            response = new ResponseEntity<>(new ProcessResult("Ops! we had a problem parsing xml"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    private boolean validateRequestInfo(RequestDto requestDto) throws InvalidDateException {
        if(requestDto != null) {
            boolean cikNotNull = requestDto.getCik() != null;
            boolean quarterIndexNotNull = requestDto.getQuarterIndex() != null;
            boolean fieldsNotNull = requestDto.getFields() != null;
            if(cikNotNull && quarterIndexNotNull && fieldsNotNull) {
                boolean inputCik = inputFilter.validateInput(requestDto.getCik().toString(), RegularExpression.VALIDATE_ONLY_NUMBER.value);
                boolean inputQuarterIndex = inputFilter.validateInput(requestDto.getQuarterIndex(), RegularExpression.VALIDATE_QUARTER_INDEX.value);
                boolean inputFields = StringUtils.isNotBlank(ArrayUtils.toString(requestDto.getFields()).replace("{", "").replace("}", ""));

                String day = requestDto.getQuarterIndex().substring(requestDto.getQuarterIndex().lastIndexOf("-") +1);
                if(Integer.parseInt(day) > 31 || Integer.parseInt(day) < 1 ){
                    throw new InvalidDateException("Invalid day informed on request");
                }
                return inputCik && inputQuarterIndex && inputFields;
            }
            return false;
        }
        return false;
    }

}

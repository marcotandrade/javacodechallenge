package com.synvata.datapointsbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DatapointsBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(DatapointsBackendApplication.class, args);
	}

}

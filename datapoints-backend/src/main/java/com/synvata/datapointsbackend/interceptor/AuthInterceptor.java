package com.synvata.datapointsbackend.interceptor;

import com.synvata.datapointsbackend.exception.InvalidTokenException;
import com.synvata.datapointsbackend.model.SystemParameter;
import com.synvata.datapointsbackend.repository.SystemParameterRepository;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Intercepts the token value informed on header 'Authorization' and validate against the table 'SYSTEM_PARAMETER'.
 *
 *
 */
public class AuthInterceptor implements HandlerInterceptor {
  private static final Logger log = LoggerFactory.getLogger(AuthInterceptor.class);

  @Autowired
  private SystemParameterRepository systemParameterRepository;

  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object o) throws Exception {

    String token = request.getHeader("Authorization");
    SystemParameter systemParameter = systemParameterRepository.findByKey("datapoint-token");

    if (StringUtils.isBlank(token) || !systemParameter.getValue().equalsIgnoreCase(token)) {
      log.error(String.format("Invalid token informed on request: %s", token));
      throw new InvalidTokenException(String.format("Invalid token informed: %s", token));
    }
    return true;
  }

}

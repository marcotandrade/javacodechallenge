package com.synvata.datapointsbackend.service;

import com.synvata.datapointsbackend.exception.ProcessingException;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;


@Service
public class RequestService {

    private static final Logger log = LoggerFactory.getLogger(RequestService.class);

    public String getRequest(String url) throws ProcessingException {
        log.debug(String.format("initialize GET request on: %s", url));

        CloseableHttpClient httpClient = HttpClients.createDefault();
        try {
            HttpGet request = new HttpGet(url);
            CloseableHttpResponse response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                return EntityUtils.toString(entity);
            }
        } catch (Exception ex) {
            throw new ProcessingException("Error during the request of execution", ex.getCause());
        }
        return null;
    }

}

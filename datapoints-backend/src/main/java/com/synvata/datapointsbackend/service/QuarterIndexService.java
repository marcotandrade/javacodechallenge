package com.synvata.datapointsbackend.service;

import com.synvata.datapointsbackend.dto.RequestDto;
import com.synvata.datapointsbackend.dto.ResponseDto;
import com.synvata.datapointsbackend.exception.InvalidDateException;
import com.synvata.datapointsbackend.exception.ProcessingException;
import com.synvata.datapointsbackend.generated.Assets;
import com.synvata.datapointsbackend.generated.Context;
import com.synvata.datapointsbackend.generated.DataPoints;
import com.synvata.datapointsbackend.generated.Deposits;
import com.synvata.datapointsbackend.generated.IncomeContext;
import com.synvata.datapointsbackend.generated.NoninterestIncome;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
public class QuarterIndexService {

    private static final Logger log = LoggerFactory.getLogger(QuarterIndexService.class);
    private static final String baseUrlIndexList = "https://www.sec.gov/Archives/edgar/full-index/";
    private static final String baseUrlFileInfo = "https://www.sec.gov/Archives/";
    private static final String ASSETS = "Assets";
    private static final String DEPOSITS = "Deposits";
    private static final String NON_INTEREST_INCOME = "NoninterestIncome";
    private static final String CONTEXT_INSTANT = "instant";

    @Autowired
    private RequestService requestService;


    /**
     * Gets the list of all files available for the requested quarter and cik
     *
     * @param requestDto
     * @return
     * @throws ProcessingException
     * @throws InvalidDateException
     */
    public String[] getIndexList(RequestDto requestDto) throws ProcessingException, InvalidDateException {
        long startTime = System.currentTimeMillis();
        log.debug("Getting index List...");
        String quarter = getQuarter(requestDto.getQuarterIndex());
        String year = requestDto.getQuarterIndex().substring(0, 4);
        String quarterReportUrl = baseUrlIndexList + year + "/" + quarter + "/" + "xbrl.idx";

        //requesting the list
        String response = requestService.getRequest(quarterReportUrl);

        //looking for target files in response
        InputStream targetStream = IOUtils.toInputStream(response, Charset.defaultCharset());
        BufferedReader rd = new BufferedReader(new InputStreamReader(targetStream));
        String line = null;

        StringBuilder message = new StringBuilder("");
        try {
            while ((line = rd.readLine()) != null) {
                if (line.contains(requestDto.getCik().toString()) && (line.contains("10-Q") || line.contains("10-K"))) {
                    message.append(line + ";");
                }
            }
        } catch (IOException ex) {
            throw new ProcessingException(String.format("Error reading response from %s", quarterReportUrl), ex);
        }
        String[] auxMessage;
        auxMessage = message.toString().split(";");
        log.debug(String.format("List extracted in %s ms", System.currentTimeMillis() - startTime ));
        return  auxMessage;
    }


    /**
     * Gets the target file and process it looking for the desired data points that matches the given quarter
     *
     * @param requestDto
     * @param fileList
     * @return
     * @throws ProcessingException
     * @throws JAXBException
     * @throws ParseException
     */
    public List<ResponseDto> getFileInfo(RequestDto requestDto, String[] fileList) throws ProcessingException, JAXBException, ParseException {
        long startTime = System.currentTimeMillis();
        log.debug("Initialize file processing...");
        List<ResponseDto> resultList = new ArrayList<>();

        for(String item : fileList){
            String filePath = item.substring(item.lastIndexOf("|") +1);
            String fileReportUrl = baseUrlFileInfo + filePath;

            //requesting the file
            String response = requestService.getRequest(fileReportUrl);
            String reportDate = getEndDate(response);

            //add already found properties on response
            setInfoOnResponse(resultList, item, fileReportUrl, reportDate);

            for(String field : requestDto.getFields()){
               String initialTagField = "<us-gaap:" + field + "\n";
               String endTagField = "</us-gaap:" + field + ">";
                String textToEval = response;
                while(textToEval.contains(initialTagField)) {

                    String xml = textToEval.substring(textToEval.indexOf(initialTagField ), textToEval.indexOf(endTagField) + endTagField.length());
                    textToEval =  textToEval.substring(textToEval.indexOf(endTagField) + endTagField.length());
                    Object parsedObj = getObjFromXml(xml);

                    String evalContext = response;
                    String initialTagContext= "<context id=\"" + getFieldValueFromClass(parsedObj,"getContextRef()") + "\""  ;
                    String endTagContext= "</context>";
                    evalContext = evalContext.substring(evalContext.indexOf(initialTagContext));
                    String contextXml = evalContext.substring(evalContext.indexOf(initialTagContext), evalContext.indexOf(endTagContext) + endTagContext.length());
                    if(StringUtils.isNotBlank(contextXml)) {
                        Object contextObj = getContextObj(contextXml);
                        if(foundMatchOnContextFields(reportDate, contextObj)){
                            //add correct dataPoints on response
                            setDataPointsOnResponse(resultList, parsedObj);
                        }
                    }
                }
            }
        }
        log.debug(String.format("File processed with success in %s ms", System.currentTimeMillis() - startTime ));
        return resultList;
    }

    private boolean foundMatchOnContextFields(String reportDate, Object contextObj) throws ParseException {

        String respIntant = getFieldValueFromClass(contextObj,"getPeriod().getInstant()");
        if(respIntant != null){
            return respIntant.equalsIgnoreCase(reportDate);
        } else{
            String startDate = getFieldValueFromClass(contextObj,"getPeriod().getStartDate()");
            String endDate = getFieldValueFromClass(contextObj,"getPeriod().getEndDate()");

            Date dateStart = new SimpleDateFormat("yyyy-MM-dd").parse(startDate);
            Date dateEnd = new SimpleDateFormat("yyyy-MM-dd").parse(endDate);
            Date dateReport = new SimpleDateFormat("yyyy-MM-dd").parse(reportDate);

            return dateReport.compareTo(dateStart) >= 0 && dateReport.compareTo(dateEnd) <= 0;
        }
    }


    private String getFieldValueFromClass(Object clazz, String method)  {
        String[] methodArray;
        methodArray = method.replace(".", ",").replace("()", "").split(",");
        try {

        for (String getMethod : methodArray) {
            Method m = clazz.getClass().getMethod(getMethod);
            Object result = m.invoke(clazz);
            clazz = result;
            if (result == null) {
                break;
            }
        }
        return clazz != null ? clazz.toString() : null;
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException ex){
            return null;
        }
    }

    private void setDataPointsOnResponse(List<ResponseDto> resultList, Object parsedObj) {
        ResponseDto response = resultList.get(resultList.size() -1);
        if(parsedObj instanceof Assets){
            response.getDataPoints().setUsGaapAssets(Integer.valueOf(Objects.requireNonNull(getFieldValueFromClass(parsedObj, "getValue()"))));
        }else if(parsedObj instanceof Deposits) {
            response.getDataPoints().setUsGaapDeposits(Integer.valueOf(Objects.requireNonNull(getFieldValueFromClass(parsedObj, "getValue()"))));
        }else {
            response.getDataPoints().setUsGaapNoninterestIncome(Integer.valueOf(Objects.requireNonNull(getFieldValueFromClass(parsedObj,
                                                                                                                              "getValue()"))));
        }
    }


    private void setInfoOnResponse(List<ResponseDto> resultList, String line, String fileReportUrl, String reportingQuarter) {
        ResponseDto responseDto = new ResponseDto();
        DataPoints dataPoints = new DataPoints();

        String[] properties = line.split("\\|");

        responseDto.setFormType(properties[2]);
        responseDto.setFileUrl(fileReportUrl);
        responseDto.setDateFiled(properties[3]);
        responseDto.setReportingQuarter(reportingQuarter);
        responseDto.setDataPoints(dataPoints);

        resultList.add(responseDto);
    }


    private Object getObjFromXml(String xml) throws JAXBException {

        JAXBContext jaxbContext;
            if(xml.contains(ASSETS)) {
                jaxbContext = JAXBContext.newInstance(Assets.class);
                Unmarshaller jaxbUnmarshallerAssets = jaxbContext.createUnmarshaller();
                return jaxbUnmarshallerAssets.unmarshal(new StringReader(xml.replace("us-gaap:", "")));
            }else if(xml.contains(DEPOSITS)) {
                jaxbContext = JAXBContext.newInstance(Deposits.class);
                Unmarshaller jaxbUnmarshallerDeposits = jaxbContext.createUnmarshaller();
                return jaxbUnmarshallerDeposits.unmarshal(new StringReader(xml.replace("us-gaap:", "")));
            }else if(xml.contains(NON_INTEREST_INCOME)) {
                jaxbContext = JAXBContext.newInstance(NoninterestIncome.class);
                Unmarshaller jaxbUnmarshallerDeposits = jaxbContext.createUnmarshaller();
                return jaxbUnmarshallerDeposits.unmarshal(new StringReader(xml.replace("us-gaap:", "")));
            }else{
                return null;
            }
        }

    private Object getContextObj(String xml) throws JAXBException {
        JAXBContext jaxbContext;
        if (xml.contains(CONTEXT_INSTANT)) {
            jaxbContext = JAXBContext.newInstance(Context.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            return jaxbUnmarshaller.unmarshal(new StringReader(xml.replace("us-gaap:", "")));
        } else {
            jaxbContext = JAXBContext.newInstance(IncomeContext.class);
            Unmarshaller jaxbUnmarshallerIncomeContext = jaxbContext.createUnmarshaller();
            return jaxbUnmarshallerIncomeContext.unmarshal(new StringReader(xml.replace("us-gaap:", "")));
        }
    }

    private String getEndDate(String response) {
        String documentPeriodEndDate = response.substring(response.indexOf("<dei:DocumentPeriodEndDate"),response.lastIndexOf("</dei:DocumentPeriodEndDate>")+ 28);
        return  documentPeriodEndDate.substring(documentPeriodEndDate.indexOf(">") +1, documentPeriodEndDate.indexOf("</dei"));
    }

    private String getQuarter(String quarterIndex) throws InvalidDateException {
        int month = Integer.parseInt(quarterIndex.substring(quarterIndex.indexOf("-") + 1, quarterIndex.lastIndexOf("-")));
        if (month < 13) {
            if (month < 4) {
                return "QTR1";
            } else if (month < 7) {
                return "QTR2";
            } else if (month < 10) {
                return "QTR3";
            } else {
                return "QTR4";
            }
        } else {
            log.error("Invalid month informed on request");
            throw new InvalidDateException("Invalid month informed on request");
        }
    }
}

package com.synvata.datapointsbackend.dto;

import java.io.Serializable;

public class ProcessResult implements Serializable {

    private Object apiResponse;

    public ProcessResult() {
    }

    public ProcessResult(Object apiResponse) {
        this.apiResponse = apiResponse;
    }

    public Object getApiResponse() {
        return apiResponse;
    }

    public void setApiResponse(Object apiResponse) {
        this.apiResponse = apiResponse;
    }
}

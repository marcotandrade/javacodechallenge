package com.synvata.datapointsbackend.dto;

import java.util.Arrays;

public class RequestDto {

    Long     cik;
    String   quarterIndex;
    String[] fields;

    public Long getCik() {
        return cik;
    }

    public void setCik(Long cik) {
        this.cik = cik;
    }

    public String getQuarterIndex() {
        return quarterIndex;
    }

    public void setQuarterIndex(String quarterIndex) {
        this.quarterIndex = quarterIndex;
    }

    public String[] getFields() {
        return fields;
    }

    public void setFields(String[] fields) {
        this.fields = fields;
    }

    @Override
    public String toString() {
        return "RequestDto{" + "cik=" + cik + ", quarterIndex='" + quarterIndex + '\'' + ", fields=" + Arrays.toString(fields) + '}';
    }
}

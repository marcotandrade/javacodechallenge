package com.synvata.datapointsbackend.dto;


import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.synvata.datapointsbackend.generated.DataPoints;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "formType",
    "dateFiled",
    "fileUrl",
    "reportingQuarter",
    "dataPoints"
})
public class ResponseDto {

    @JsonProperty("formType")
    private String              formType;
    @JsonProperty("dateFiled")
    private String              dateFiled;
    @JsonProperty("fileUrl")
    private String              fileUrl;
    @JsonProperty("reportingQuarter")
    private String              reportingQuarter;
    @JsonProperty("dataPoints")
    private DataPoints          dataPoints;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("formType")
    public String getFormType() {
        return formType;
    }

    @JsonProperty("formType")
    public void setFormType(String formType) {
        this.formType = formType;
    }

    @JsonProperty("dateFiled")
    public String getDateFiled() {
        return dateFiled;
    }

    @JsonProperty("dateFiled")
    public void setDateFiled(String dateFiled) {
        this.dateFiled = dateFiled;
    }

    @JsonProperty("fileUrl")
    public String getFileUrl() {
        return fileUrl;
    }

    @JsonProperty("fileUrl")
    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    @JsonProperty("reportingQuarter")
    public String getReportingQuarter() {
        return reportingQuarter;
    }

    @JsonProperty("reportingQuarter")
    public void setReportingQuarter(String reportingQuarter) {
        this.reportingQuarter = reportingQuarter;
    }

    @JsonProperty("dataPoints")
    public DataPoints getDataPoints() {
        return dataPoints;
    }

    @JsonProperty("dataPoints")
    public void setDataPoints(DataPoints dataPoints) {
        this.dataPoints = dataPoints;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("formType", formType).append("dateFiled", dateFiled).append("fileUrl", fileUrl).append("reportingQuarter", reportingQuarter).append("dataPoints", dataPoints).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(formType).append(reportingQuarter).append(dataPoints).append(fileUrl).append(additionalProperties).append(dateFiled).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof ResponseDto) == false) {
            return false;
        }
        ResponseDto rhs = ((ResponseDto) other);
        return new EqualsBuilder().append(formType, rhs.formType).append(reportingQuarter, rhs.reportingQuarter).append(dataPoints, rhs.dataPoints).append(fileUrl, rhs.fileUrl).append(additionalProperties, rhs.additionalProperties).append(dateFiled, rhs.dateFiled).isEquals();
    }

}

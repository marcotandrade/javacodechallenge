package com.synvata.datapointsbackend.repository;

import com.synvata.datapointsbackend.model.SystemParameter;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface SystemParameterRepository extends PagingAndSortingRepository<SystemParameter, Long> {

    SystemParameter findByKey(String key);

}

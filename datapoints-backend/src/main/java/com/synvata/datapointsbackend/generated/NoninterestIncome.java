//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2020.03.22 às 03:34:20 PM AMT 
//


package com.synvata.datapointsbackend.generated;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;simpleContent>
 *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>float">
 *       &lt;attribute name="contextRef" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="decimals" type="{http://www.w3.org/2001/XMLSchema}byte" />
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="unitRef" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/simpleContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "value"
})
@XmlRootElement(name = "NoninterestIncome")
public class NoninterestIncome {

    @XmlValue
    protected Integer value;
    @XmlAttribute(name = "contextRef")
    protected String contextRef;
    @XmlAttribute(name = "decimals")
    protected Byte decimals;
    @XmlAttribute(name = "id")
    protected String id;
    @XmlAttribute(name = "unitRef")
    protected String unitRef;

    /**
     * Obtém o valor da propriedade value.
     * 
     */
    public Integer getValue() {
        return value;
    }

    /**
     * Define o valor da propriedade value.
     * 
     */
    public void setValue(Integer value) {
        this.value = value;
    }

    /**
     * Obtém o valor da propriedade contextRef.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContextRef() {
        return contextRef;
    }

    /**
     * Define o valor da propriedade contextRef.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContextRef(String value) {
        this.contextRef = value;
    }

    /**
     * Obtém o valor da propriedade decimals.
     * 
     * @return
     *     possible object is
     *     {@link Byte }
     *     
     */
    public Byte getDecimals() {
        return decimals;
    }

    /**
     * Define o valor da propriedade decimals.
     * 
     * @param value
     *     allowed object is
     *     {@link Byte }
     *     
     */
    public void setDecimals(Byte value) {
        this.decimals = value;
    }

    /**
     * Obtém o valor da propriedade id.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Define o valor da propriedade id.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Obtém o valor da propriedade unitRef.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnitRef() {
        return unitRef;
    }

    /**
     * Define o valor da propriedade unitRef.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnitRef(String value) {
        this.unitRef = value;
    }

}

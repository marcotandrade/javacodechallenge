package com.synvata.datapointsbackend.generated;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "us-gaap:Assets",
    "us-gaap:Deposits",
    "us-gaap:NoninterestIncome"
})
public class DataPoints {

    @JsonProperty("us-gaap:Assets")
    private Integer             usGaapAssets;
    @JsonProperty("us-gaap:Deposits")
    private Integer             usGaapDeposits;
    @JsonProperty("us-gaap:NoninterestIncome")
    private Integer             usGaapNoninterestIncome;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("us-gaap:Assets")
    public Integer getUsGaapAssets() {
        return usGaapAssets;
    }

    @JsonProperty("us-gaap:Assets")
    public void setUsGaapAssets(Integer usGaapAssets) {
        this.usGaapAssets = usGaapAssets;
    }

    @JsonProperty("us-gaap:Deposits")
    public Integer getUsGaapDeposits() {
        return usGaapDeposits;
    }

    @JsonProperty("us-gaap:Deposits")
    public void setUsGaapDeposits(Integer usGaapDeposits) {
        this.usGaapDeposits = usGaapDeposits;
    }

    @JsonProperty("us-gaap:NoninterestIncome")
    public Integer getUsGaapNoninterestIncome() {
        return usGaapNoninterestIncome;
    }

    @JsonProperty("us-gaap:NoninterestIncome")
    public void setUsGaapNoninterestIncome(Integer usGaapNoninterestIncome) {
        this.usGaapNoninterestIncome = usGaapNoninterestIncome;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("usGaapAssets", usGaapAssets).append("usGaapDeposits", usGaapDeposits).append("usGaapNoninterestIncome", usGaapNoninterestIncome).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(usGaapAssets).append(usGaapNoninterestIncome).append(additionalProperties).append(usGaapDeposits).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof DataPoints) == false) {
            return false;
        }
        DataPoints rhs = ((DataPoints) other);
        return new EqualsBuilder().append(usGaapAssets, rhs.usGaapAssets).append(usGaapNoninterestIncome, rhs.usGaapNoninterestIncome).append(additionalProperties, rhs.additionalProperties).append(usGaapDeposits, rhs.usGaapDeposits).isEquals();
    }


}

# datapoints-backend

Import the maven project in the folder `datapoints-backend`

- Build it using `mvn clean install`
- Run the class `DatapointsBackendApplication` as Java Application to see it working

---

## Execution 

- The project exposes a single endpoint `localhost:9092/data-points/ws` with a GET method </br>

- It requires a token that has to be informed on Authorization header

- Token: fcb48542afc719ea5f4fb0333b37774f2296799282d184af6d338a8dc09ad9c924b77fb996c95fc1e759f3d70f1705b3999e5cfae43a27615415cad04e25d395

Especified port 9092 is configured on application.yml as well as the application context. </br>

---

## Project Info & Frameworks Used

- Spring Boot 
- Java 8
- Apache HttpComponents (to perform request)
- jaxb-api (to generate class objects from xml string)
- H2 in memory database
- Spring data JPA

 


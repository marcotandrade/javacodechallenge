import React from 'react';
import AppNavbar from './components/appNavbar'; 
import SampleForm from './components/sampleForm'; 
import './App.css';

function App() {
  return (
    <div className="App">
       <AppNavbar/>
       <SampleForm/>
    </div>
  );
}

export default App;

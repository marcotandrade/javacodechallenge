import React, { Component } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import AccountCircle from '@material-ui/icons/AccountCircle';
import { Container } from '@material-ui/core';


const styles = {
  logo: {
    width: 250

  },
  rightContainer: {
    paddingRight: 0
  },
  buttonContainer: {
    marginRight: 0,
    paddingRight: 0,
    display: "none"
  },
  button: {
    float: "right",
    color: "inherit"
  }
}


export default class AppNavbar extends Component {

  render() {
    return (

       <div >
        <AppBar position="static">
          <Toolbar>
            <div style={styles.logo}>
              <img src={require('./assets/synvata.jpeg')} width="100" height="100" alt="Synvata" />
            </div>


            <Container id="titleContainer">
              <Typography variant="h6">
                Consult Company Data Points
              </Typography>
            </Container>
            <Container id="rightContainer" style={styles.rightContainer}>
            <Container id="toolBar"  style={styles.buttonContainer}>
              <IconButton
                style={styles.button}
                onClick={this.handleAccountClick}
              >
                <AccountCircle />
              </IconButton>
            </Container>
            </Container>
          </Toolbar>
        </AppBar>
      </div>


    );
  }
}

import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import axios from 'axios';


const datapointbackend = {
    method: 'get',
    url: 'http://localhost:9092/data-points/ws',
    headers: { 'Authorization': 'fcb48542afc719ea5f4fb0333b37774f2296799282d184af6d338a8dc09ad9c924b77fb996c95fc1e759f3d70f1705b3999e5cfae43a27615415cad04e25d395' }
}



export default class SampleForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
          errorMessage: "",
          hasError: false,
          quarterIndex: "", 
          cik: "", 
          fields: [],
        };
      }


    render() {
            
            return (
                <div>
                <Typography variant="h6" gutterBottom>
                </Typography>
                    <Grid container spacing={3} justify="center" alignItems="center">
                            <Grid item xs={2} sm={2}>
                                <TextField
                                    required
                                    id="CIK"
                                    name="CIK"
                                    label="CIK "
                                    value={this.state.cik}
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={2} sm={2}>
                                <TextField
                                    required
                                    id="quaterIndex"
                                    name="quaterIndex"
                                    label="Quarter Index"
                                    value={this.state.quarterIndex}
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={2} sm={2}>
                                <TextField
                                    required
                                    id="fields"
                                    name="fields"
                                    label="Fields"
                                    fullWidth
                                />
                            </Grid>
                            
                    </Grid>
                    <Grid container spacing={3} justify="center" alignItems="center">
                        <Grid item xs={2} sm={2}>
                                    <Button variant="contained" color="primary"  onClick={this.handleSearchButtonClick()}>
                                        Request
                                        </Button>
                                </Grid>

                    </Grid>

                 
                </div>
            );
        }

        handleSearchButtonClick = (update = false) => event => {
            this.search(this.state.cik, this.state.quarterIndex, this.state.fields);
          }

          

        async search(cik, quarterIndex, fields) {
            let res = await axios.get(datapointbackend);
            

          }
}